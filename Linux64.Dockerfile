FROM ubuntu:kinetic
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y install wget libunwind8 libicu-dev libdw-dev

RUN wget https://packages.microsoft.com/config/ubuntu/22.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN rm packages-microsoft-prod.deb

RUN apt-get update && apt-get -y install dotnet-sdk-7.0